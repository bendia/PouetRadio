#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  PouetRadio.py
#  
#  Copyright 2017 Benoît FORQUIN <b.forquin@pansebete.net>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

#Dépendances : Mastodon.py
#CLIENT_ID : fichier généré par
#Mastodon.create_app(
     #'pytooterapp',
     #api_base_url="https://ma_super_instance.mastodon",
      #to_file = 'pytooter_clientcred.secret'
#)
#ACCESS_TOKEN généré par
#mastodon = Mastodon(client_id = 'pytooter_clientcred.secret')
#mastodon.log_in(
    #'my_login_email@example.com',
    #'incrediblygoodpassword',
    #api_base_url="https://ma_super_instance.mastodon",
    #to_file = 'pytooter_usercred.secret'
#)
#M3U-FILE : chemin du fichier m3u généré
#Usage : pouetradio.py CLIENT_ID ACCESS_TOKEN M3U_FILE

from mastodon import Mastodon
import arrow
import urllib
from lxml.html import etree
import requests
import configparser
import os
import sys

try:
    from tkinter import *
    GUI = True
except ImportError:
    GUI = False

class Conf():
    def __init__(self,config_file="pouetconf.ini"):
        
        
        self.conf= configparser.ConfigParser()
        self.conf.read(config_file)
        self.instanceURL = self.checkOption('Default', 'INSTANCE_URL', checkFile = False)
        self.client_id = self.checkOption('Credentials', 'CLIENT_ID')
        self.access_token = self.checkOption('Credentials', 'CLIENT_ID')
        self.rememberMe = False
        self.login = None
        self.password = None
        
        
        
        self.mastodon = Mastodon(client_id = self.client_id, access_token = self.access_token)
        
        self.m3u_file = self.conf.get('Playlist', 'M3U_FILE')
        
            
    def checkOption(self, section, option, checkFile = True):
        try:
            check = self.conf.get(section, option)
        except (NoSectionError, NoOptionError) as e:
            check = None
        
        if checkFile:
            if os.path.isfile(check):
                return check
            else :
                return None
        else:
            return check
        
            
class Cli():
    def __init__(self, conf):
        self.conf = conf
    
    def instanceURL(self):
        self.conf.instanceURL = input("Entrez l'adresse de votre instance Mastodon :\n")
            
    def clientCredFilePath(self):
        self.conf.client_id = input("Saisissez le chemin complet de votre fichier clé d'application pyPouetradio :\n")
        
    def accessToken(self):
        self.conf.login = input("Adresse e-mail de connexion à votre instance : \n")
        self.conf.password = input("Mot de passe :\n")
        if input("Conserver un fichier d'accès pour vos connexions futures ? o/n :\n") == "o":
            self.conf.access_token = input("Saisissez le chemin complet de votre fichier jeton de connexion :\n")
            self.conf.rememberMe = True
            
        
        


template='''
#EXTREM {} {}@{} a partagé
{}

'''
m=Conf()
#~ mastodon = Mastodon(
    #~ client_id = CLIENT_ID,
    #~ access_token = ACCESS_TOKEN
#~ )

def generate_posts(tag):
    max_id = None
    while True:
        posts = m.mastodon.timeline_hashtag(tag, max_id=max_id, limit=20)
        for post in posts:
            yield post
        max_id = post['id']

def analyse_links(links):
    for link in links:
        href = link.attrib["href"]
        if all([cond not in href for cond in ["tag", "@", "media"]]):
            resp = requests.get(href)
            if resp.ok: #and urllib.parse.urlparse(resp.url).netloc == "www.youtube.com":
                yield resp.url

playlist=[]
for i, post in enumerate(generate_posts("pouetradio")):
    display_name = post["account"]["display_name"]
    user_name = post["account"]["username"]
    content = post["content"]
    date = post["created_at"]
    count = post["reblogs_count"]
    try:
        link = next(analyse_links(etree.HTML(content).xpath("//a")))
        with open(m.m3u_file, 'a') as m3u:
            playlist.append(template.format(arrow.get(date).format('Le DD-MM-YYYY à HH:mm'),display_name,user_name,link)) 
        
    except StopIteration:
        pass
    if i >= 20:
        break

if os.path.isfile(m.m3u_file):
    os.remove(m.m3u_file)

        
with open(m.m3u_file,"a") as m3u:
    m3u.write("#EXTM3U")
    for line in playlist:
        m3u.write(line)
    
    
